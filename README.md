# Tiny NeXT Workstation

These are the support files for the [Tiny NeXT Workstation](https://transmutable.com/work/electronic-tiny-next-workstation) created by Trevor Flowers.

This document also could be useful for anyone using an [AdaFruit QT Py / XIAO](https://www.adafruit.com/category/595) and one of the [EyeSPY displays](https://www.adafruit.com/category/63).

The eM3 comes with these specific boards:
- [QT Py ESP32-S3](https://www.adafruit.com/product/5426)
- [EyeSPI BFF](https://www.adafruit.com/product/5772)
- [2.2" 18-bit color TFT LCD display](https://www.adafruit.com/product/1480) 

Here are the excellent Adafruit learning guides for these boards:
- [QT Py](https://learn.adafruit.com/adafruit-qt-py-esp32-s3)
- [BFF](https://learn.adafruit.com/adafruit-eyespi-bff) 
- [display guide](https://learn.adafruit.com/2-2-tft-display)

The circuit-python-src directory in this repo constains several example scripts, including the default script that ships on the device, a clock script that uses the WiFi chip, and a script that turns on every pixel so that you can see the active area of the display.

Hot tip: Open up those guides when you're programming as they demonstrate how to use All The Things in both CircuitPython and Arduino.



