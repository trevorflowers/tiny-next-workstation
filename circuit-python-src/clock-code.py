import os
import ssl
import rtc
import wifi
import time
import board
import pwmio
import ipaddress
import displayio
import terminalio
import socketpool

import adafruit_ntp
import adafruit_requests
from adafruit_ili9341 import ILI9341
from adafruit_display_text import label

print("Starting NeXTcube demo")

tft_cs = board.TX
tft_dc = board.RX

timezone = -7
font_scale = 8
line_height = 40
left_margin = 50
font_color = 0xFFFFFF
background_color = 0x000000

real_time_clock = rtc.RTC()

displayio.release_displays()
spi = board.SPI()

display_bus = displayio.FourWire(
    spi, command=tft_dc,
    chip_select=tft_cs,
    reset=None
)

# Set up the display
display = ILI9341(display_bus, width=320, height=240)
splash = displayio.Group()
display.show(splash)
color_bitmap = displayio.Bitmap(320, 240, 1)
color_palette = displayio.Palette(1)
color_palette[0] = background_color
bg_sprite = displayio.TileGrid(color_bitmap, pixel_shader=color_palette, x=0, y=0)
splash.append(bg_sprite)

# Set up networking
wifi.radio.connect(os.getenv('CIRCUITPY_WIFI_SSID'), os.getenv('CIRCUITPY_WIFI_PASSWORD'))
pool = socketpool.SocketPool(wifi.radio)
print("MAC addr:", [hex(i) for i in wifi.radio.mac_address])
print("IP address:", wifi.radio.ipv4_address)

def draw_label(txt, scale, color, x, y):
    text_group = displayio.Group(scale=scale, x=x, y=y)
    text_area = label.Label(terminalio.FONT, text=txt, color=color)
    text_group.append(text_area)
    splash.append(text_group)

def draw_time(dt):
    time_string = "{:02}:{:02}".format(dt.tm_hour, dt.tm_min)
    draw_label(time_string, font_scale, font_color, left_margin, 120)

print("Setup complete")

last_hour = -1
last_minute = -1
while True:
    if real_time_clock.datetime.tm_hour != last_hour:
        real_time_clock.datetime = adafruit_ntp.NTP(pool, tz_offset=timezone).datetime
        last_hour = real_time_clock.datetime.tm_hour
    if real_time_clock.datetime.tm_min is not last_minute:
        splash.pop()
        draw_time(real_time_clock.datetime)
        last_minute = real_time_clock.datetime.tm_min
    time.sleep(5)


