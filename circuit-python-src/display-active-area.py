import os
import time
import board
import pwmio
import displayio
import terminalio

import adafruit_ntp
import adafruit_requests
from adafruit_ili9341 import ILI9341
from adafruit_display_text import label

print("Starting NeXTcube demo")

tft_cs = board.TX
tft_dc = board.RX

font_scale = 6
line_height = 60
font_color = 0xFFFFFF
background_color = 0x449944
left_margin = 40
sleep_between_text = 0.2

displayio.release_displays()
spi = board.SPI()

display_bus = displayio.FourWire(
    spi, command=tft_dc,
    chip_select=tft_cs,
    reset=None
)

# Set up the display
display = ILI9341(display_bus, width=320, height=240)
splash = displayio.Group()
display.show(splash)
color_bitmap = displayio.Bitmap(320, 240, 1)
color_palette = displayio.Palette(1)
color_palette[0] = background_color
bg_sprite = displayio.TileGrid(color_bitmap, pixel_shader=color_palette, x=0, y=0)
splash.append(bg_sprite)

def draw_label(txt, scale, color, x, y):
    '''Draw a single line into the display context, splash.'''
    text_group = displayio.Group(scale=scale, x=x, y=y)
    text_area = label.Label(terminalio.FONT, text=txt, color=color)
    text_group.append(text_area)
    splash.append(text_group)

def draw_text_array(txt=[]):
    '''Write a series of lines of text to the display context, splash'''
    for i in range(1, len(txt) + 1):
        draw_label(txt[i - 1], font_scale, font_color, left_margin, i * line_height)
        time.sleep(sleep_between_text)

# Set up an array of text (you can change it to whatever you want!)
text_lines = [
    "Active",
    "Display",
    "Area",
]

draw_text_array(text_lines)


print("Setup complete")

while True:
    time.sleep(1000)
    continue

